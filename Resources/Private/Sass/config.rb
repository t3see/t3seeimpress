relative_assets = true

sass_dir = "."
css_dir = "../../Public/Css"
images_dir = "../../Public/Images"
fonts_dir = "../../Public/Fonts"
javascripts_dir = "../../Public/JavaScript"

# One of: nested, expanded, compact, compressed
output_style = :compact
# One of: development (default), production
environment = :production